// ################################
// Zoe Bogner (Instructure) 6 Jan 2021
// 0.1, Chris Battista (DoE) 13 Jan 2021
// 0.2. Christopher McAvaney (Instructure) 31 Mar 2022
// 0.3 - final version prior to PROD - Christopher McAvaney (Instructure) 20220520
// 0.4 - modifying "acceptable use policy" logo - Christopher McAvaney (Instructure) 20220602
// 0.5 - modifying footer links - Christopher McAvaney (Instructure) 20220621
// 0.6 - modifying footer links to final correct - Tori Carroll and Christopher McAvaney (Instructure) 20220707
// 0.7 - hard coding beta tool id for atomic search beta widget testing - Hannah Mefford (Atomic Jolt) 20221031
//     - additional dynamic mappings - Christopher McAvaney (Instructure) 20221102
// 0.8 - removing yellow banner from qed03-stage (aka Champions instance) - Christopher McAvaney (Instructure) 20221108
// 0.10 - Switching to Beta search widget and enabling openEQUELLA feature - Ryker Blunck (Atomic Jolt) 20230227
// 0.11 - new enhanced Atomic Jolt products - Atomic Search and Atomic Author 20230824
// ################################

if (typeof jQuery == 'undefined' || typeof jQuery === undefined || typeof jQuery === null) {
	var headTag = document.getElementsByTagName("head")[0];
	var jqTag = document.createElement('script');
	jqTag.type = 'text/javascript';
	jqTag.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js';
	headTag.appendChild(jqTag);
	jqTag.onload = myJQueryCode;
} else {
	myJQueryCode();
}

function myJQueryCode() {
	(function() {
		'use strict';

		var _aup_links = '<a class="terms_link" href="https://education.qld.gov.au/curriculum/qlearn/acceptable-use-policy/conditions-of-use" target="_blank">Conditions of Use</a> | <a class="terms_link" href="https://education.qld.gov.au/curriculum/qlearn/acceptable-use-policy/privacy-statement" target="_blank">Privacy</a> | <a class="terms_link" href="https://education.qld.gov.au/curriculum/qlearn/acceptable-use-policy/copyright-statement" target="_blank">Copyright</a>';

		// Footer links
		var footerHTML = '<div id="footer-links" class="ic-app-footer__links" style="justify-content: center;">' + _aup_links + '</div>';

		// Footer links with login page formatting
		var footerLoginHTML = '<div id="footer-links" class="ic-Login-footer__links"><a class="support_url help_dialog_trigger external" data-track-category="help system" data-track-label="help button" href="http://help.instructure.com/" target="_blank" rel="noreferrer noopener"><span>Help</span><span class="ui-icon ui-icon-extlink ui-icon-inline" title="Links to an external site."><span class="screenreader-only">Links to an external site.</span></span></a> | ' + _aup_links + '</div>';


		// Dashboard (and anywhere else the footer is already displayed)
		if ($('#footer') && (document.location.pathname != "/login/canvas")) {
			$('#footer-links').replaceWith(footerHTML);
		}

		// Canvas login page
		if ($('#footer') && (document.location.pathname === "/login/canvas")) {
			$('#footer-links').replaceWith(footerLoginHTML);
		}

		// Modify the logo in the acceptable use policy screen header
		$('header.ic-Login-confirmation__header').attr('style', 'background-color: #0b2846');

		// QLearn cropped SVG - inline as it is simpler than trying to find somewhere to host this file
		const image_svg=`
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   version="1.1"
   id="svg2"
   xml:space="preserve"
   width="147.40134"
   height="49.133865"
   viewBox="0 0 147.40134 49.133865"
   sodipodi:docname="QLearn-namestyle Cropped DD 221014.ai"><metadata
     id="metadata8"><rdf:RDF><cc:Work
         rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" /></cc:Work></rdf:RDF></metadata><defs
     id="defs6"><clipPath
       clipPathUnits="userSpaceOnUse"
       id="clipPath18"><path
         d="M 0,36.85 H 110.551 V 0 H 0 Z"
         id="path16" /></clipPath></defs><sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="640"
     inkscape:window-height="480"
     id="namedview4" /><g
     id="g10"
     inkscape:groupmode="layer"
     inkscape:label="QLearn-namestyle Cropped DD 221014"
     transform="matrix(1.3333333,0,0,-1.3333333,0,49.133867)"><g
       id="g12"><g
         id="g14"
         clip-path="url(#clipPath18)"><g
           id="g20"
           transform="translate(104.1924,-2e-4)"><path
             d="m 0,0 h -97.833 c -3.513,0 -6.359,2.847 -6.359,6.358 v 24.134 c 0,3.512 2.846,6.359 6.359,6.359 H 0 c 3.512,0 6.358,-2.847 6.358,-6.359 V 6.358 C 6.358,2.847 3.512,0 0,0"
             style="fill:#0a2946;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path22" /></g><g
           id="g24"
           transform="translate(19.2085,24.9676)"><path
             d="m 0,0 c -0.368,1.168 -1.607,2.937 -4.085,2.937 -1.575,0 -2.948,-0.801 -3.618,-2.036 -0.803,-1.502 -1.172,-3.538 -1.172,-6.342 0,-4.005 0.67,-6.508 2.077,-7.51 0.736,-0.533 1.674,-0.801 2.779,-0.801 3.248,0 4.622,2.47 4.622,8.378 C 0.603,-3.004 0.402,-1.302 0,0 m 7.2,-20.895 c -0.134,-0.066 -0.837,-0.4 -2.076,-0.4 -1.809,0 -3.416,1.067 -4.287,1.636 -1.105,0.733 -2.913,1.768 -4.722,2.336 -3.483,0 -5.76,0.969 -7.534,3.271 -1.776,2.27 -2.68,5.206 -2.68,8.611 0,5.007 1.909,9.045 5.258,10.848 1.373,0.734 3.081,1.135 4.822,1.135 6.196,0 10.014,-4.506 10.014,-11.783 0,-5.039 -1.809,-8.811 -4.588,-10.647 0.635,-0.234 1.54,-0.835 2.076,-1.168 0.837,-0.501 1.842,-0.968 2.88,-0.968 0.904,0 1.875,0.133 2.076,0.167 z"
             style="fill:#966fb0;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path26" /></g><g
           id="g28"
           transform="translate(41.5117,8.0115)"><path
             d="m 0,0 h -12.29 v 23.197 h 4.789 V 3.872 h 8.338 z"
             style="fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path30" /></g><g
           id="g32"
           transform="translate(54.3042,18.6253)"><path
             d="m 0,0 c 0,1.269 -0.134,1.936 -0.536,2.57 -0.435,0.668 -1.071,1.002 -1.975,1.002 -1.708,0 -2.679,-1.335 -2.679,-3.705 V -0.2 H 0 Z m -5.258,-3.471 v -0.133 c 0,-2.638 1.306,-4.14 3.617,-4.14 1.54,0 2.981,0.568 4.353,1.702 l 1.743,-2.669 c -1.977,-1.602 -4.053,-2.37 -6.464,-2.37 -4.923,0 -8.104,3.471 -8.104,8.845 0,3.071 0.636,5.107 2.143,6.776 1.407,1.568 3.115,2.303 5.391,2.303 1.976,0 3.852,-0.668 4.957,-1.803 1.574,-1.602 2.277,-3.905 2.277,-7.476 v -1.035 z"
             style="fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path34" /></g><g
           id="g36"
           transform="translate(70.2783,15.4881)"><path
             d="m 0,0 c -3.114,0 -4.186,-0.567 -4.186,-2.604 0,-1.334 0.837,-2.236 1.976,-2.236 0.837,0 1.674,0.435 2.344,1.168 L 0.201,0 Z M 3.818,-8.344 C 2.78,-7.91 1.842,-7.143 1.406,-6.274 1.071,-6.608 0.703,-6.942 0.368,-7.176 c -0.837,-0.6 -2.042,-0.934 -3.449,-0.934 -3.817,0 -5.894,1.935 -5.894,5.339 0,4.006 2.78,5.875 8.239,5.875 0.335,0 0.635,0 1.005,-0.033 v 0.7 c 0,1.903 -0.37,2.538 -2.01,2.538 -1.441,0 -3.114,-0.702 -4.956,-1.936 l -1.909,3.204 c 0.904,0.567 1.574,0.901 2.779,1.402 1.675,0.701 3.114,1.001 4.688,1.001 2.88,0 4.857,-1.068 5.527,-2.971 0.233,-0.7 0.334,-1.235 0.3,-3.071 l -0.1,-5.741 c -0.033,-1.869 0.1,-2.67 1.607,-3.804 z"
             style="fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path38" /></g><g
           id="g40"
           transform="translate(88.4961,21.1956)"><path
             d="m 0,0 c -0.436,0.234 -0.804,0.334 -1.306,0.334 -1.005,0 -1.909,-0.467 -2.746,-1.436 V -13.184 H -8.54 v 11.415 c 0,2.303 -0.268,4.039 -0.636,5.007 l 4.019,1.068 c 0.402,-0.701 0.636,-1.469 0.703,-2.437 0.972,1.302 2.345,2.437 4.018,2.437 0.67,0 0.972,-0.067 1.675,-0.368 z"
             style="fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path42" /></g><g
           id="g44"
           transform="translate(101.0215,8.0115)"><path
             d="m 0,0 v 11.115 c 0,1.936 -0.335,2.503 -1.508,2.503 -0.904,0 -2.076,-0.601 -3.114,-1.535 V 0 h -4.487 v 12.383 c 0,1.469 -0.202,2.87 -0.603,4.005 l 3.984,1.135 c 0.403,-0.701 0.637,-1.435 0.637,-2.136 0.67,0.467 1.239,0.868 1.976,1.268 0.904,0.468 2.076,0.735 3.081,0.735 1.909,0 3.583,-1.002 4.119,-2.47 C 4.319,14.286 4.42,13.551 4.42,12.483 V 0 Z"
             style="fill:#ffffff;fill-opacity:1;fill-rule:nonzero;stroke:none"
             id="path46" /></g></g></g></g></svg>
`.trim();
		$('#modal-box > header img.ic-Login-confirmation__logo').replaceWith(image_svg);
		$('#modal-box > header svg#svg2').attr('preserveAspectRatio', 'xMinYMin meet');
		$('#modal-box > header svg#svg2').attr('height', '34px');
		$('#modal-box > header').css('padding-left', '0px');


		// Staging specific
		// ignoring qed03-stage - i.e. '3' isn't in the set of numbers
		var qed_stage_pattern = new RegExp('^qed(0[0-24-9])?-stage', 'i');
		if (qed_stage_pattern.test(location.host)) {
			$('div#application').after('<div id="qdoe-staging-banner">Staging environment</div>');
		}

	})();
}

var atomicSearchConfig = {
  accountId: 1, // The ID of your root account (usually 1)
  hasEquella: true
};

var atomicSearchWidgetScript = document.createElement("script");
atomicSearchWidgetScript.src = "https://js.atomicsearchwidget.com/atomic_search_widget.js";
document.getElementsByTagName("head")[0].appendChild(atomicSearchWidgetScript);

