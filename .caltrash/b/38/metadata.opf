<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">38</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">41a641d0-0b46-4e64-a112-d21a21a1f7ee</dc:identifier>
        <dc:title>Julius Caesar: The Life and Times of the People’s Dictator</dc:title>
        <dc:creator opf:file-as="Canfora, Luciano &amp; Midgley, Stuart &amp; Hill, Marian &amp; Windle, Kevin" opf:role="aut">Luciano Canfora</dc:creator>
        <dc:creator opf:file-as="Canfora, Luciano &amp; Midgley, Stuart &amp; Hill, Marian &amp; Windle, Kevin" opf:role="aut">Stuart Midgley</dc:creator>
        <dc:creator opf:file-as="Canfora, Luciano &amp; Midgley, Stuart &amp; Hill, Marian &amp; Windle, Kevin" opf:role="aut">Marian Hill</dc:creator>
        <dc:creator opf:file-as="Canfora, Luciano &amp; Midgley, Stuart &amp; Hill, Marian &amp; Windle, Kevin" opf:role="aut">Kevin Windle</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (7.10.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>0101-01-01T00:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;
&lt;p&gt;In this splendid profile, Luciano Canfora offers a radically new interpretation of one of the most controversial figures in history. Julius Caesar played a leading role in the culture and politics of a world empire, dwarfing his contemporaries in ambition, achievement, and appetite. For that, he has occupied a central place in the political imagination ever since. Yet Caesar, struck down by his own lieutenants because he could not be comprehended nor contained, remains an enigma. The result of a comprehensive study of the ancient sources, &lt;em&gt;Julius Caesar: The Life and Times of the People's Dictator&lt;/em&gt; paints an astonishingly detailed portrait of this complex man and the times in which he lived. Based on his many years of research, Canfora focuses on what we actually know about Caesar, the man of politics and war, in a stylish, engaging narrative chronologically structured around the events in Caesar's life. The result is a rich, revelatory, full biographical portrait of the dictator whose mission of Romanization lies at the very heart of modern Europe. &lt;/p&gt;
&lt;p style="font-style: italic"&gt;Copub: Edinburgh University Press&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:identifier opf:scheme="ISBN">9780520235021</dc:identifier>
        <dc:identifier opf:scheme="AMAZON">0520235029</dc:identifier>
        <dc:language>eng</dc:language>
        <meta name="calibre:timestamp" content="2024-06-03T09:15:40.051000+00:00"/>
        <meta name="calibre:title_sort" content="Julius Caesar: The Life and Times of the People’s Dictator"/>
    </metadata>
    <guide>
        <reference type="cover" title="Cover" href="cover.jpg"/>
    </guide>
</package>
